/***************************/
/* FILE NAME: LEX_FILE.lex */
/***************************/

/***************************/
/* AUTHOR: OREN ISH SHALOM */
/***************************/

/*************/
/* USER CODE */
/*************/
   
import java_cup.runtime.*;

/******************************/
/* DOLAR DOLAR - DON'T TOUCH! */
/******************************/
      
%%
   
/************************************/
/* OPTIONS AND DECLARATIONS SECTION */
/************************************/

/*****************************************************/ 
/* Lexer is the name of the class JFlex will create. */
/* The code will be written to the file Lexer.java.  */
/*****************************************************/ 
%class Lexer

%cupsym TokenNames

%eofval{
		return symbol(TokenNames.EOF);
%eofval}


/********************************************************************/
/* The current line number can be accessed with the variable yyline */
/* and the current column number with the variable yycolumn.        */
/********************************************************************/
%line
%column
/******************************************************************/
/* CUP compatibility mode interfaces with a CUP generated parser. */
/******************************************************************/
%cup
   
/****************/
/* DECLARATIONS */
/****************/
/*****************************************************************************/   
/* Code between %{ and %}, both of which must be at the beginning of a line, */
/* will be copied letter to letter into the Lexer class code.                */
/* Here you declare member variables and functions that are used inside the  */
/* scanner actions.                                                          */  
/*****************************************************************************/   
%{   
    /*********************************************************************************/
    /* Create a new java_cup.runtime.Symbol with information about the current token */
    /*********************************************************************************/
    private Symbol symbol(int type)               {return new Symbol(type, yyline, yycolumn);}
    private Symbol symbol(int type, Object value) {return new Symbol(type, yyline, yycolumn, value);}
	{yyline++;}
%}

/***********************/
/* MACRO DECALARATIONS */
/***********************/
LineTerminator	= \r|\n|\r\n
WhiteSpace		= {LineTerminator} | [ \t\f]
INTEGER			= 0 | [1-9][0-9]*
ID				=[a-z][A-Za-z_0-9]*	//original: [A-Za-z_][A-Za-z_0-9]*
CLASS_ID			=[A-Z][A-Za-z_0-9]*
COMMENT			= [/][\*][^]*[\*][/]
LineComment		= [/]+.*
QUOTE			= [\"]+.*+[\"]
/******************************/
/* DOLAR DOLAR - DON'T TOUCH! */
/******************************/

%%

/************************************************************/
/* LEXER matches regular expressions to actions (Java code) */
/************************************************************/
   
/**************************************************************/
/* YYINITIAL is the state at which the lexer begins scanning. */
/* So these regular expressions will only be matched if the   */
/* scanner is in the start state YYINITIAL.                   */
/**************************************************************/
   
<YYINITIAL> {

";"					{System.out.print(yyline+":");
					 System.out.print("SEMICOLON"); System.out.println(); return symbol(TokenNames.SEMICOLON);}
"+"					{System.out.print(yyline+":");
					 System.out.print("PLUS");      System.out.println(); return symbol(TokenNames.PLUS);}
"-"					{System.out.print(yyline+":");
					 System.out.print("MINUS");      System.out.println(); return symbol(TokenNames.MINUS);}
"*"					{ System.out.print(yyline+":");
					System.out.print("TIMES");     System.out.println(); return symbol(TokenNames.TIMES);}
"/"					{ System.out.print(yyline+":");
						System.out.print("DIVIDE");    System.out.println(); return symbol(TokenNames.DIVIDE);}
"("					{System.out.print(yyline+":"); 
					System.out.print("LPAREN");    System.out.println(); return symbol(TokenNames.LPAREN);}

"="				{ System.out.print(yyline+":");
						System.out.print("ASSIGN");    System.out.println(); return symbol(TokenNames.ASSIGN);}
"=="					{ System.out.print(yyline+":");
						System.out.print("EQUAL");    System.out.println(); return symbol(TokenNames.EQUAL);}
"boolean"					{ System.out.print(yyline+":");
						System.out.print("BOOLEAN");    System.out.println(); return symbol(TokenNames.BOOLEAN);}
"break"					{ System.out.print(yyline+":");
						System.out.print("BREAK");    System.out.println(); return symbol(TokenNames.BREAK);}
"class"					{ System.out.print(yyline+":");
						System.out.print("CLASS");    System.out.println(); return symbol(TokenNames.CLASS);}
","					{ System.out.print(yyline+":");
						System.out.print("COMMA");    System.out.println(); return symbol(TokenNames.COMMA);}
"continue"					{ System.out.print(yyline+":");
						System.out.print("CONTINUE");    System.out.println(); return symbol(TokenNames.CONTINUE);}
"."					{ System.out.print(yyline+":");
						System.out.print("DOT");    System.out.println(); return symbol(TokenNames.DOT);}
"extends"					{ System.out.print(yyline+":");
						System.out.print("EXTENDS");    System.out.println(); return symbol(TokenNames.EXTENDS);}
"else"					{ System.out.print(yyline+":");
						System.out.print("ELSE");    System.out.println(); return symbol(TokenNames.ELSE);}
"false"					{ System.out.print(yyline+":");
						System.out.print("FALSE");    System.out.println(); return symbol(TokenNames.FALSE);}
">"					{ System.out.print(yyline+":");
						System.out.print("GT");    System.out.println(); return symbol(TokenNames.GT);}
">="					{ System.out.print(yyline+":");
						System.out.print("GTE");    System.out.println(); return symbol(TokenNames.GTE);}

"if"					{ System.out.print(yyline+":");
						System.out.print("IF");    System.out.println(); return symbol(TokenNames.IF);}
"int"					{ System.out.print(yyline+":");
						System.out.print("INT");    System.out.println(); return symbol(TokenNames.INT);}
						
"&&"					{ System.out.print(yyline+":");
						System.out.print("LAND");    System.out.println(); return symbol(TokenNames.LAND);}
"["					{ System.out.print(yyline+":");
						System.out.print("LBRACK");    System.out.println(); return symbol(TokenNames.LBRACK);}
"{"					{ System.out.print(yyline+":");
						System.out.print("LBRACE");    System.out.println(); return symbol(TokenNames.LBRACE);}
//"LENGTH"					{ System.out.print(yyline+":");
//						System.out.print("LENGTH");    System.out.println(); return symbol(TokenNames.LENGTH);}
"new"					{ System.out.print(yyline+":");
						System.out.print("NEW");    System.out.println(); return symbol(TokenNames.NEW);}
"!"					{ System.out.print(yyline+":");
						System.out.print("LNEG");    System.out.println(); return symbol(TokenNames.LNEG);}
"||"					{ System.out.print(yyline+":");
						System.out.print("LOR");    System.out.println(); return symbol(TokenNames.LOR);}
"<"					{ System.out.print(yyline+":");
						System.out.print("LT");    System.out.println(); return symbol(TokenNames.LT);}
"<="					{ System.out.print(yyline+":");
						System.out.print("LTE");    System.out.println(); return symbol(TokenNames.LTE);}
"%"					{ System.out.print(yyline+":");
						System.out.print("MOD");    System.out.println(); return symbol(TokenNames.MOD);}
"!="					{ System.out.print(yyline+":");
						System.out.print("NEQUAL");    System.out.println(); return symbol(TokenNames.NEQUAL);}
"null"					{ System.out.print(yyline+":");
						System.out.print("NULL");    System.out.println(); return symbol(TokenNames.NULL);}
"]"					{ System.out.print(yyline+":");
						System.out.print("RBRACK");    System.out.println(); return symbol(TokenNames.RBRACK);}
"}"					{ System.out.print(yyline+":");
						System.out.print("RBRACE");    System.out.println(); return symbol(TokenNames.RBRACE);}
"return"					{ System.out.print(yyline+":");
						System.out.print("RETURN");    System.out.println(); return symbol(TokenNames.RETURN);}
")"					{ System.out.print(yyline+":");
						System.out.print("RPAREN");    System.out.println(); return symbol(TokenNames.RPAREN);}
"static"					{ System.out.print(yyline+":");
						System.out.print("STATIC");    System.out.println(); return symbol(TokenNames.STATIC);}
"string"					{ System.out.print(yyline+":");
						System.out.print("STRING");    System.out.println(); return symbol(TokenNames.STRING);}
"this"					{ System.out.print(yyline+":");
						System.out.print("THIS");    System.out.println(); return symbol(TokenNames.THIS);}
"true"					{ System.out.print(yyline+":");
						System.out.print("TRUE");    System.out.println(); return symbol(TokenNames.TRUE);}
"void"					{ System.out.print(yyline+":");
						System.out.print("VOID");    System.out.println(); return symbol(TokenNames.VOID);}
"while"					{ System.out.print(yyline+":");
						System.out.print("WHILE");    System.out.println(); return symbol(TokenNames.WHILE);}
"\""					{ System.out.print(yyline+":");
						System.out.print("QUOTE");    System.out.println(); return symbol(TokenNames.QUOTE);}
"main"					{ System.out.print(yyline+":");
						System.out.print("MAIN");    System.out.println(); return symbol(TokenNames.MAIN);}

<<EOF>>     			{ System.out.print((yyline+1)+":");
						System.out.print("EOF");    System.out.println()	;return symbol(TokenNames.EOF);}
{COMMENT}			{}
{LineComment}		{}	
{QUOTE}				{
						System.out.print(yyline+":");
						System.out.print("QUOTE(");
						System.out.print(yytext());
						System.out.print(")");
						System.out.println(); return symbol(TokenNames.QUOTE, new String(yytext()));
}
{INTEGER}			{
						System.out.print(yyline+":");
						System.out.print("INT(");
						System.out.print(yytext());
						System.out.print(")");
						System.out.println(); return symbol(TokenNames.NUMBER, new Integer(yytext()));
					}   
					
{CLASS_ID}			{
						System.out.print(yyline+":");
						System.out.print("CLASS_ID("+yytext()+")");    
						System.out.println(); 
						return symbol(TokenNames.CLASS_ID,new String(yytext()));
}

{ID}		{
						System.out.print(yyline+":");
						System.out.print("ID(");
						System.out.print(yytext());
						System.out.print(") ");
						System.out.println(); 
						return symbol(TokenNames.ID, new String(yytext()));
					}
{WhiteSpace}		{ /* just skip what was found, do nothing */ }   
[^]                 {System.out.print(yyline+": Lexical error: illegal character '"+yytext()+"'");
					System.out.println("Error!"); return symbol(TokenNames.ERR);
					}
}
