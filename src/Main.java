
   
import java.io.*;

import java_cup.runtime.Symbol;
import AST.*;
import Main.SemanticAnalayzer;

public class Main
{
	static public void main(String argv[])
	{
		Lexer l;
		Parser p;
		Symbol s;
		FileReader file_reader;
		PrintWriter file_writer;
		String inputFilename = argv[0];
		String outputFilename = argv[1];
		
		try
		{
			/********************************/
			/* [1] Initialize a file reader */
			/********************************/
 			file_reader = new FileReader(inputFilename);

			/********************************/
			/* [2] Initialize a file writer */
			/********************************/
			file_writer = new PrintWriter(outputFilename);
			
		
//			AST_STMT_LIST stmtList = (AST_STMT_LIST) p.parse().value;
			//SemanticAnalayzer analayzer = new SemanticAnalayzer();
			//try{
				/******************************/
				/* [3] Initialize a new lexer */
				/******************************/
				l = new Lexer(file_reader);
				
				/*******************************/
				/* [4] Initialize a new parser */
				/*******************************/
				p = new Parser(l);

				/********************************/
				/* [5] Main reading tokens loop */
				/********************************/
				AST_PROGRAM programTree = (AST_PROGRAM) p.parse().value;
				try{
					programTree.isValid();
					SemanticAnalayzer.test();
					programTree.buildIR();
					file_writer.write("OK");
				}catch(Exception e){
					file_writer.write("FAIL");
					file_writer.close();
					e.printStackTrace();

				}
				/*
			}catch(Exception e){
				file_writer.write("FAIL");
				
			}
			*/
//			while (stmtList != null)
//			{
//				System.out.print(stmtList.PrintMe());
//				System.out.print("\n");				
//				stmtList = stmtList.tail;
//			}
			
			/**************************/
			/* [10] Close output file */
			/**************************/
			file_writer.close();
    	}
			     
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}


