
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sun Jan 01 21:30:27 IST 2017
//----------------------------------------------------

/** CUP generated interface containing symbol constants. */
public interface TokenNames {
  /* terminals */
  public static final int DIVIDE = 23;
  public static final int LTE = 43;
  public static final int LPAREN = 24;
  public static final int CONTINUE = 15;
  public static final int INT = 2;
  public static final int MINUS = 21;
  public static final int STATIC = 8;
  public static final int RPAREN = 25;
  public static final int SEMICOLON = 38;
  public static final int LT = 41;
  public static final int COMMA = 32;
  public static final int CLASS = 36;
  public static final int ERR = 50;
  public static final int PLUS = 20;
  public static final int MULTIPLY = 16;
  public static final int MAIN = 51;
  public static final int ASSIGN = 30;
  public static final int QUOTE = 10;
  public static final int IF = 18;
  public static final int THIS = 9;
  public static final int DOT = 19;
  public static final int ID = 34;
  public static final int EOF = 0;
  public static final int BOOLEAN = 4;
  public static final int RETURN = 39;
  public static final int LAND = 17;
  public static final int EQUAL = 42;
  public static final int TRUE = 12;
  public static final int NEW = 47;
  public static final int error = 1;
  public static final int LOR = 3;
  public static final int NULL = 48;
  public static final int NUMBER = 33;
  public static final int MOD = 14;
  public static final int BREAK = 6;
  public static final int VOID = 49;
  public static final int GTE = 45;
  public static final int LBRACK = 26;
  public static final int TIMES = 40;
  public static final int LBRACE = 28;
  public static final int ELSE = 5;
  public static final int RBRACK = 27;
  public static final int WHILE = 22;
  public static final int NEQUAL = 46;
  public static final int CLASS_ID = 35;
  public static final int RBRACE = 29;
  public static final int EXTENDS = 37;
  public static final int STRING = 31;
  public static final int LNEG = 11;
  public static final int FALSE = 13;
  public static final int GT = 44;
  public static final int LENGTH = 7;
}

