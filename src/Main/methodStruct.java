package Main;
import java.util.List;

import AST.AST_EXP_LIST;


public class methodStruct{
	public String name;
	public String returnType;
	public String labelName;
	List<String> argsType;
	public methodStruct(String n, String rt, List<String> argt){
		this.name = n;
		this.returnType = rt;
		this.argsType = argt;
		this.labelName = "label_" + n + "_" + SemanticAnalayzer.labelCounter;
		SemanticAnalayzer.labelCounter++;
	}
	public boolean compareArgs(List<String> argList) {
		if(argList==null && argsType==null) 
			return true;
		if(argList==null || argsType==null)
			return false;
		if(this.argsType.size()!=argList.size())
			return false;
		for(int i=0;i<this.argsType.size();i++){
			if(!SemanticAnalayzer.compareTypes(this.argsType.get(i), argList.get(i)))
				return false;
		}
		return true;
	}
}