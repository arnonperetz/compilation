package Main;
import java.util.LinkedList;
import java.util.List;

public class HashMap {
      public final static int TABLE_SIZE = 128;
      HashEntry[] table;
 
      HashMap() {
            table = new HashEntry[TABLE_SIZE];
            for (int i = 0; i < TABLE_SIZE; i++)
                  table[i] = null;
      }
      private int hashCode(String key){
          int hash = (key.hashCode() % TABLE_SIZE);
          if(hash<0)
          	hash+=TABLE_SIZE;
          return hash;
      }
      public HashEntry get(String key) {
    	  	int hash=hashCode(key);
			if(table[hash]==null)
				return null;
			HashEntry curr = table[hash];
			while(curr!=null&&!curr.name.equals(key)){
				curr=curr.next;
			}
			return curr;
	  }
 
      public void put(String key, HashEntry value) {
            int hash = (key.hashCode() % TABLE_SIZE);
            if(hash<0)
            	hash+=TABLE_SIZE;
			if(table[hash]!=null){
				HashEntry prev = table[hash];
				table[hash] = value;
				value.next=prev;
			}else{
				table[hash] = value;			
			}
      }
      //Removes the first element in the linked list 'table[key]'
      public void remove(String key){
    	  int hash=hashCode(key);
    	  if(table[hash]==null)
    		  return;
    	  table[hash]=table[hash].next;
      }

	public void putNewEntry(String class_id) {
		ClassHashEntry entry = new ClassHashEntry(class_id,null);
		put(entry.name,entry);
	}

	public void update(String key, HashEntry value) {
		int hash = (key.hashCode() % TABLE_SIZE);
		if(hash<0)
        	hash+=TABLE_SIZE;
		if(table[hash]!=null){
			HashEntry curr = table[hash];
			HashEntry prev=null;
			while(curr!=null && !curr.name.equals(key)){
				prev=curr;
				curr=curr.next;
			}
			if(curr!=null){
				if(prev==null){
					value.next=null;
					table[hash]=value;
				}
				else{
					prev.next=value;
					value.next=curr.next;
				}
			}
		}
	}
}