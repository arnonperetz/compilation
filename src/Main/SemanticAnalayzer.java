package Main;

import java.util.LinkedList;
import java.util.List;

public class SemanticAnalayzer {
	public static SymbolTable symbolTable = new SymbolTable();
	public static HashMap classEntries = new HashMap();
	public static int labelCounter = 0;
	public static List<String> framesVars = new LinkedList<>();
	
	public SemanticAnalayzer(){
		symbolTable = new SymbolTable();
		classEntries = new HashMap();
	}
	public static boolean compareTypesForClassInstitaion(String l, String r){
		if(l.equals(r))
			return true;
		ClassHashEntry entryr = (ClassHashEntry) SemanticAnalayzer.classEntries.get(r);
		String nextr=r;
		String nextl=l;
		if(entryr.extendsFrom!=null) 
			nextr=entryr.extendsFrom;
		if(entryr.extendsFrom==null) 
			return false;
		return compareTypes(nextl, nextr);
	}

	public static boolean compareTypes(String l, String r){
		if(l.equals(r))
			return true;
		ClassHashEntry entryl = (ClassHashEntry) SemanticAnalayzer.classEntries.get(l);
		ClassHashEntry entryr = (ClassHashEntry) SemanticAnalayzer.classEntries.get(r);
		String nextr=r;
		String nextl=l;
		if(entryl.extendsFrom!=null) 
			nextl=entryl.extendsFrom;
		if(entryr.extendsFrom!=null) 
			nextr=entryr.extendsFrom;
		if(entryl.extendsFrom==null && entryr.extendsFrom==null) 
			return false;
		return compareTypes(nextl, nextr);
	}
	public static void createNewClassEntry(String class_id) {
		classEntries.putNewEntry(class_id);
	}


	public static void CopyClassBody(String class_id, String extend_class) {
		ClassHashEntry entry= (ClassHashEntry) classEntries.get(extend_class);
		classEntries.put(class_id, entry);
		ClassHashEntry entry2= (ClassHashEntry) classEntries.get(class_id);
		entry2.name=class_id;
		entry2.extendsFrom=extend_class;

	}


	public static void addFieldToClassEntry(String symbol, String type,
			String class_id) {
		ClassHashEntry entry= (ClassHashEntry) classEntries.get(class_id);
		fieldStruct fs = new fieldStruct(symbol,type);
		entry.fieldsList.add(fs);
		classEntries.update(class_id,entry);
	}


	public static void addMethodToClassEntry(String name, String returnType,
			List<String> formals, String class_id) {
		ClassHashEntry entry= (ClassHashEntry) classEntries.get(class_id);
		methodStruct ms = new methodStruct(name,returnType,formals);
		entry.methodList.add(ms);
		classEntries.update(class_id,entry);		
	}
	public static fieldStruct getFiledFromClassEntry(String class_id, String fieldName) {
		ClassHashEntry classEntry = (ClassHashEntry)classEntries.get(class_id);
		if(classEntry==null)
			return null;
		for(fieldStruct field :classEntry.fieldsList){
			if(field.name.equals(fieldName))
				return field;
		}
		return null;
	}


	public static methodStruct getMethodFromClassEntry(String class_id, String methodName) {
		ClassHashEntry classEntry = (ClassHashEntry)classEntries.get(class_id);
		if(classEntry==null)
			return null;
		for(methodStruct method :classEntry.methodList){
			if(method.name.equals(methodName))
				return method;
		}
		return null;
		
	}


	public static boolean isValidType(String type) {
		if(type.equals("INT") || type.equals("STRING"))
			return true;
		return classEntries.get(type)!=null;
	}
	
	public static void test(){
		
	}
	public static void startFrameScope() {
		SemanticAnalayzer.framesVars.add(0,"$");
		
	}
	public static void endFrameScope() {
		while(!SemanticAnalayzer.framesVars.get(0).equals("$"))
			SemanticAnalayzer.framesVars.remove(0);
	}
	public static void addVarToFrame(String var){
		SemanticAnalayzer.framesVars.add(0,var);
	}

}
