package Main;
import java.util.LinkedList;
import java.util.List;

public class SymbolTable{
	HashMap symbols;
	List<String> stack;
	public SymbolTable(){
		symbols=new HashMap();
		stack = new LinkedList<String>();
		startScope();
	}
	public void add(SymbolHashEntry symbol){
		stack.add(symbol.name);
		symbols.put(symbol.name,symbol);
	}
	public void startScope(){
		stack.add(0,"$");
	}
	public void endScope(){
		String s=stack.get(0);
		while(!s.equals("$")){
			stack.remove(0);
			symbols.remove(s);
			s=stack.get(0);
		}
		stack.remove(0);
	}
	public String find(String name){
		SymbolHashEntry entry= (SymbolHashEntry) symbols.get(name);
		if(entry==null)
			return null;
		return entry.type;
	}
}