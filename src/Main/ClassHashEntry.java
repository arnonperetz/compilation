package Main;
import java.util.LinkedList;
import java.util.List;

public class ClassHashEntry extends HashEntry{
	  //public String name;				    	//in derived class
      public List<methodStruct> methodList;
	  public List<fieldStruct> fieldsList;
	  public String extendsFrom=null;
	  //public ClassHashEntry next=null;	    //in derived class
      ClassHashEntry(String n, ClassHashEntry next) {
            this.name = n;
            this.methodList = new LinkedList<methodStruct>();
			this.fieldsList = new LinkedList<fieldStruct>();
			this.next = next;
      }     
	  void addMethod(methodStruct m){
		  this.methodList.add(0, m);
	  }
	  void addField(fieldStruct f){
		  this.fieldsList.add(0, f);
	  }
	  
}