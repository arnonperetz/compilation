package AST;
import java.util.ArrayList;
import java.util.List;

import IRNodes.*;

public class AST_PROGRAM extends AST_Node{
	List<AST_CLASS_DECLARATION> classDeclList;
	
	public AST_PROGRAM(AST_CLASS_DECLARATION cd){
		classDeclList = new ArrayList<AST_CLASS_DECLARATION>();
		this.addDecl(cd);
	}
	public AST_PROGRAM(AST_CLASS_DECL_LIST cdl) {
		classDeclList = new ArrayList<AST_CLASS_DECLARATION>();
		classDeclList=cdl.list;
	}
	private void addDecl(AST_CLASS_DECLARATION cd) {
		this.classDeclList.add(cd);
	}
	public boolean isValid() {
		for(AST_CLASS_DECLARATION classDecl : classDeclList){
			classDecl.addToClassTable();
		}
		for(AST_CLASS_DECLARATION classDecl : classDeclList){
			if(!classDecl.isValid(classDecl.class_id))
				throw new RuntimeException();
		}
		
		return true;
	}
	public void buildIR() {
		T_exp root = new T_Seq(getNextSeq(0), new T_Label("exit label"));
	}
	T_exp getNextSeq(int index){
		return new T_Seq(classDeclList.get(index).buildIR(),getNextSeq(index++));
	}
	
}
