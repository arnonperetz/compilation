package AST;

import java.util.ArrayList;

import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_TYPE_AND_NAME extends AST_METHOD {
	AST_TYPE t; String name;
	public AST_METHOD_TYPE_AND_NAME(AST_TYPE t, String name) {
		this.t=t;
		this.name=name;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, t.getType(), new ArrayList<String>(),class_id);
	}
	/*always valid*/
	public boolean isValid(){
		return true;
	}
	/*return type*/
	public String getType(){
		return t.getType();
	}
}
