package AST;

import Main.SemanticAnalayzer;

public class AST_EXP_BINOP extends AST_EXP
{
	public AST_BINOP_IMPL OP;
	public AST_EXP left;
	public AST_EXP right;
	
	/******************/
	/* CONSTRUCTOR(S) */
	/******************/
	public AST_EXP_BINOP(AST_EXP left,AST_EXP right,AST_BINOP_IMPL OP)
	{
		this.left = left;
		this.right = right;
		this.OP = OP;
	}

	public boolean isValid(String class_id) {
		//children invalid
		if((left.isValid(class_id)==false )||( right.isValid(class_id)==false))
		{
			throw new RuntimeException();
		}
		//right&left not from the same type 
		String rightType = right.getType();
		String leftType = left.getType();
		
		if (!SemanticAnalayzer.compareTypes(rightType,leftType))
		{
			throw new RuntimeException();
		}

		if(OP.getType().equals("PLUS") || OP.getType().equals("MINUS") || OP.getType().equals("TIMES") || OP.getType().equals("DIVIDE"))
		{
			// two ints
			if(SemanticAnalayzer.compareTypes(rightType,leftType))
				return true;
			throw new RuntimeException();
		}
		
		if(OP.getType().equals("LT") || OP.getType().equals("LTE") || OP.getType().equals("GT") || OP.getType().equals("GTE")|| OP.getType().equals("EQUAL")|| OP.getType().equals("NEQUAL"))
		{
			if(SemanticAnalayzer.compareTypes(rightType,leftType))
				return true;
		}
		throw new RuntimeException();
	}
	
	public String getType(){
		if((left.getType()==null )||( right.getType()==null))
		{
			return null;
		}
		//right&left not from the same type 
		if (!(right.getType().equals(left.getType())))
		{
			return null;
		}

		if(OP.getType().equals("PLUS") || OP.getType().equals("MINUS") || OP.getType().equals("TIMES") || OP.getType().equals("DIVIDE"))
		{
			// two ints
			if(left.getType().equals("INT") && right.getType().equals("INT"))
				return "INT";
			return null;
		}
		
		if(OP.getType().equals("LT") || OP.getType().equals("LTE") || OP.getType().equals("GT") || OP.getType().equals("GTE"))
		{
			// two ints
			if(left.getType().equals("INT") && right.getType().equals("INT"))
				return "INT";
			// two strings
			else if (left.getType().equals("STRING") && right.getType().equals("STRING"))
				return "STRING";
			return null;
		}
		return null;
	}
	
	
}