package AST;
import Main.SemanticAnalayzer;
public class AST_METHOD_MAIN extends AST_METHOD {
	AST_FORMALS f; AST_STMT_LIST l;
	public AST_METHOD_MAIN(AST_FORMALS f, AST_STMT_LIST l) {
		// TODO Auto-generated constructor stub
		this.f=f;
		this.l=l;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry("main", "void", f.getFormals(),class_id);
	}
	/*opens scope
	 * checks formals and statement list
	 * end scope
	 * return the value */
	public boolean isValid(String class_id){
		SemanticAnalayzer.symbolTable.startScope();
		boolean rv = f.isValid(class_id)&&l.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
		
	}
	/*no type for main . return "void"*/
	public String getType(){
		return "void";
	}
}
