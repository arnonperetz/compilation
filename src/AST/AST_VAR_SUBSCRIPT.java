package AST;

public class AST_VAR_SUBSCRIPT extends AST_VAR
{
	public String name;
	public AST_EXP subscript;
	
	/******************/
	/* CONSTRUCTOR(S) */
	/******************/
	public AST_VAR_SUBSCRIPT(String name,AST_EXP subscript)
	{
		this.name = name;
		this.subscript = subscript;
	}
}