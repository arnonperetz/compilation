package AST;
import Main.SemanticAnalayzer;
//!!!!
public class AST_LOCATION_ID extends AST_LOCATION {
	String id;
	public AST_LOCATION_ID(String id) {
		this.id=id;
	}
	public boolean isValid(String class_id) {
		//the location exist in symbolTable
		if(SemanticAnalayzer.symbolTable.find(this.id)!=null) return true;
		throw new RuntimeException();
	}

	public String getType() {
		return SemanticAnalayzer.symbolTable.find(this.id);
	}

}
