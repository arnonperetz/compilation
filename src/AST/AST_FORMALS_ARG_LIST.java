package AST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

//!!!!
public class AST_FORMALS_ARG_LIST extends AST_FORMALS {
	String name; AST_TYPE t; AST_ARG_LIST l;
	public AST_FORMALS_ARG_LIST(String name, AST_TYPE t, AST_ARG_LIST l) {
		this.name=name;
		this.t=t;
		this.l=l;
	}
	public List<String> getFormals(){
		List<String> rv = new ArrayList<String>();
		rv.add(t.getType());
		rv.addAll(l.getFormals());
		return rv;
	}
	public Map<String,String> getArgList(){
		Map<String,String> rv = new HashMap<String,String>();
		for(AST_ARG arg:l.list)
			rv.put(arg.name, arg.t.getType());
		return rv;
	}
	
	public boolean isValid(String class_id){
		SymbolHashEntry symbol = new SymbolHashEntry(name,t.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
				//AST_ARG_LIST adds the symbols to the symbols table
		if(this.l.isValid(class_id)) return true;
		throw new RuntimeException();

	}
}
