package AST;

public class AST_BINOP_IMPL extends AST_Node{
	int op;
	public AST_BINOP_IMPL(int op){
		this.op=op;
	}
	
	public boolean isValid(){
		return true;
	}
	
	public String getType(){
		switch(op){
		case 0:
			return "MINUS";
		case 1:
			return "PLUS";
		case 2:
			return "TIMES";
		case 3:
			return "DIVIDE";
		case 4:
			return "LT";
		case 5:
			return "LTE";
		case 6:
			return "GT";
		case 7:
			return "GTE";
		case 8:
			return "EQUAL";
		case 9:
			return "NEQUAL";
		}
		return null;
	}
}


