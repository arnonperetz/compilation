package AST;

import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_TYPE_AND_FORMALS extends AST_METHOD {
	AST_TYPE t; String name; AST_FORMALS f;
	public AST_METHOD_TYPE_AND_FORMALS(AST_TYPE t, String name, AST_FORMALS f) {
		this.t=t;
		this.name=name;
		this.f=f;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, t.getType(), f.getFormals(),class_id);
	}
	/* opens scope
	 * checks formals
	 * end scope
	 * return the value */
	public boolean isValid(String class_id){
		SemanticAnalayzer.symbolTable.startScope();
		boolean rv = f.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	/*returns type*/
	public String getType(){
		return t.getType();
	}
}
