package AST;

import java.util.List;

import Main.HashMap;
import Main.SemanticAnalayzer;
import Main.methodStruct;

//!!!!
public class AST_VIRTUAL_CALL_FULL extends AST_VIRTUAL_CALL {
	String name; AST_EXP_LIST l;AST_LOCATION loc;
	private String className;
	public AST_VIRTUAL_CALL_FULL(String name, AST_EXP_LIST l) {
		// TODO Auto-generated constructor stub
		this.name=name;
		this.l=l;
		this.loc=null;
	}

	public AST_VIRTUAL_CALL_FULL(AST_LOCATION loc, String name, AST_EXP_LIST l) {
		// TODO Auto-generated constructor stub
		this.name=name;
		this.l=l;
		this.loc=loc;
	}
	public boolean isValid(String class_id) {
		this.className=class_id;
		if(loc!=null)
			className=loc.getType();
		//HashMap ClassEntries =  SemanticAnalayzer.classEntries;
		methodStruct ms= SemanticAnalayzer.getMethodFromClassEntry(className, this.name);
		if(ms==null)
			throw new RuntimeException();
		if(l!=null){
			if(ms.compareArgs(this.l.getArgsType())&&l.isValid(class_id) && loc.isValid(class_id)) return true;
		}else{
			List<String> a = null;
			if(ms.compareArgs(a) && loc.isValid(class_id)) return true;
		}
		throw new RuntimeException();
	}

	public String getType() {
		if(loc!=null)
			className=loc.getType();
		methodStruct ms= Main.SemanticAnalayzer.getMethodFromClassEntry(className, this.name);
		if(ms==null)
			return null;
		return ms.returnType;
	}
}
