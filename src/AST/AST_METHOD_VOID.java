package AST;

import java.util.ArrayList;

import IRNodes.T_exp;
import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_VOID extends AST_METHOD {

	String name;
	public AST_METHOD_VOID(String name) {
		// TODO Auto-generated constructor stub
		this.name=name;
	}
	public void addToClassEntries(String class_id) {
		SemanticAnalayzer.addMethodToClassEntry(this.name, "void", new ArrayList<String>(),class_id);
	}
	/*always true ?*/
	public boolean isValid(){
		SemanticAnalayzer.symbolTable.startScope();
		SemanticAnalayzer.symbolTable.endScope();

		return true;
	}
	/*return "void"*/
	public String getType(){
		return "void";
	}
	public T_exp buildIR(){
		SemanticAnalayzer.startFrameScope();
		SemanticAnalayzer.endFrameScope();
		return null;
	}
}
