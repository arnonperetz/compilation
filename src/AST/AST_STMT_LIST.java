package AST;

import java.util.ArrayList;
import java.util.List;

public class AST_STMT_LIST extends AST_Node
{
	/****************/
	/* DATA MEMBERS */
	/****************/
	public AST_STMT head;
	public AST_STMT_LIST tail;
	List<AST_STMT> list;
	/******************/
	/* CONSTRUCTOR(S) */
	/******************/
	public AST_STMT_LIST(){};
	public AST_STMT_LIST(AST_STMT head,AST_STMT_LIST tail)
	{
		this.head = head;
		this.tail = tail;
	}

	public AST_STMT_LIST(AST_STMT s) {
		list = new ArrayList<AST_STMT>();
		Add(s);
	}

	public void Add(AST_STMT s) {
		list.add(s);
		// TODO Auto method stub
		
	}
	public boolean isValid(String class_id){
		boolean rv = true;
		if(list!=null)
		for(int i=0;i<list.size();i++)
			rv = rv && this.list.get(i).isValid(class_id);
		if(rv) return true;
		throw new RuntimeException();
		/*
		boolean rv = head.isValid();
		AST_STMT_LIST curr= tail;
		while(curr!=null){
			rv = rv & curr.head.isValid();
			curr=curr.tail;
		}
		return rv;
		*/
	}
			
}
