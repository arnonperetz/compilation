package AST;

public class AST_LOCATION_EXP_ARRAY extends AST_LOCATION {
	AST_LOCATION loc1; Integer num;
	public AST_LOCATION_EXP_ARRAY(AST_LOCATION loc1, Integer num) {
		// TODO Auto-generated constructor stub
		this.loc1=loc1;
		this.num=num;
	}
	public boolean isValid(String class_id) {
		if(this.loc1.getType()!=null) return true;
		throw new RuntimeException();
	}

	public String getType() {
		String type = this.loc1.getType();
		if(type!=null && type.contains("[]"))
			return type.replaceFirst("[]", "");
		return null;
	}
}
