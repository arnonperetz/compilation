package AST;
//!!!!
public class AST_CALL_IMPL extends AST_CALL {
	AST_VIRTUAL_CALL vc;
	public AST_CALL_IMPL(AST_VIRTUAL_CALL vc) {
		this.vc=vc;
	}
	
	public boolean isValid(String class_id){
		if(vc.isValid(class_id)) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return vc.getType();
	}

}
