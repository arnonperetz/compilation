package AST;

import java.util.Arrays;

import Main.ClassHashEntry;
import Main.HashEntry;
import Main.HashMap;
import Main.SemanticAnalayzer;

//!!!!
public class AST_LITERAL_IMPL extends AST_LITERAL{
	int i;
	boolean isInt=false;
	String s=null;
	boolean isString=false;
	public AST_LITERAL_IMPL(int i) {
		this.i=i;
		this.isInt=true;
	}
	public AST_LITERAL_IMPL(String s) {
		this.s=s;
		this.isString=true;
	}
	
	public boolean isValid(String class_id) {
		return true;
	}
	
	public String getType(){
		if(isInt)
			return "INT";
		else if(isString){
				return "STRING";
		}
		return null;
	}
	
}
