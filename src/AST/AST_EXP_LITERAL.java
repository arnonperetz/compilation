package AST;
//!!!!
public class AST_EXP_LITERAL extends AST_EXP {
	AST_LITERAL l;
	public AST_EXP_LITERAL(AST_LITERAL l) {
		this.l=l;
	}

	public boolean isValid(String class_id) {
		if(l.isValid(class_id)) return true;
		throw new RuntimeException();
	}
	
	public String getType() {
		// TODO Auto-generated method stub
		return l.getType();
	}
	

}
