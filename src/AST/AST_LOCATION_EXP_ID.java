package AST;

import Main.SemanticAnalayzer;

//!!!!
public class AST_LOCATION_EXP_ID extends AST_LOCATION {
	AST_LOCATION loc1=null; AST_LOCATION loc2=null;String id=null;String id1=null;
	int locationType;
	public AST_LOCATION_EXP_ID(AST_LOCATION loc1, AST_LOCATION loc2) {
		this.loc1=loc1;
		this.loc2=loc2;
		//array
	}
	public AST_LOCATION_EXP_ID(String id,String id1) {
		this.id=id;
		this.id1=id1;
		//locationType=1;
	}
	public AST_LOCATION_EXP_ID(AST_LOCATION loc, String id) {
		this.loc1=loc;
		this.id=id;
		//locationType=2;
	}
	public boolean isValid(String class_id) {
		if(this.getType()!=null) return true;
		throw new RuntimeException();
		//return this.getType()!=null;		//todo!
	}

	public String getType() {
		String class_id;
		String fieldName = null;
		//array
		if(this.loc1!=null && this.loc2!=null){
			String type = this.loc1.getType();
			if(!type.contains("[]")){
				return type.replaceFirst("[]", "");
			}
			else{
				return null;
			}
		}
		// a.b
		else if(this.id!=null && this.id1!=null){
			class_id = Main.SemanticAnalayzer.symbolTable.find(this.id);
			fieldName = this.id1;
		}
		else if(this.loc1!=null && this.id!=null){
			class_id=this.loc1.getType();
			fieldName = this.id;
		}
		else{
			return null;
		}
		
		return Main.SemanticAnalayzer.getFiledFromClassEntry(class_id, fieldName).type;		//todo!
		//return SemanticAnalayzer.symbolTable.find(this.id);
	}
}
