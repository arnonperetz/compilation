package AST;

public class AST_VAR_FIELD extends AST_VAR
{
	public String name;
	public String fieldName;
	
	/******************/
	/* CONSTRUCTOR(S) */
	/******************/
	public AST_VAR_FIELD(String name,String fieldName)
	{
		this.name = name;
		this.fieldName = fieldName;
	}
}