package AST;
//!!!!
public class AST_EXP_NEW_TYPE extends AST_EXP {

	AST_TYPE t; AST_EXP e;
	public AST_EXP_NEW_TYPE(AST_TYPE t, AST_EXP e) {
		this.t=t;
		this.e=e;
	}
	/*
	public AST_EXP_NEW_TYPE(AST_TYPE t2) {
		this.t=t2;
	}
	*/
	public boolean isValid(String class_id){
		if(!e.getType().equals("INT"))
			throw new RuntimeException();;
		if(Main.SemanticAnalayzer.isValidType(t.getType())) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return t.getType()+"[]";
	}


}
