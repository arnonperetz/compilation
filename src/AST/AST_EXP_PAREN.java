package AST;
//!!!!
public class AST_EXP_PAREN extends AST_EXP {
	AST_EXP e;
	public AST_EXP_PAREN(AST_EXP e) {
		this.e=e;
	}

	public boolean isValid(String class_id) {
		if(e.isValid(class_id)) return true;
		throw new RuntimeException();
	}
}
