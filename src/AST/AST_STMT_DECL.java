package AST;
import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;
//!!!!
public class AST_STMT_DECL extends AST_STMT {
	AST_TYPE t; String id;
	public AST_STMT_DECL(AST_TYPE t, String id) {
		this.t=t;
		this.id=id;
	}
	public boolean isValid(){
		SymbolHashEntry symbol = new SymbolHashEntry(this.id,t.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
		return true;
	}

}
