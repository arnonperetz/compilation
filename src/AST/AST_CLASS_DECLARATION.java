package AST;
import IRNodes.T_exp;
import Main.SemanticAnalayzer;

public class AST_CLASS_DECLARATION extends AST_Node{
	String class_id;
	String extend_class=null;
	AST_CLASS_BODY_LIST classBody;
	public void addToClassTable() {
		SemanticAnalayzer.createNewClassEntry(this.class_id);
		if(this.extend_class!=null)
			SemanticAnalayzer.CopyClassBody(this.class_id,this.extend_class);
		classBody.addClassBodyToHashEntry(this.class_id);		
	}
	
	public boolean isValid(String class_id) {
		SemanticAnalayzer.symbolTable.startScope();
		boolean rv = classBody.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	
	public String getType(){
		return this.class_id;
	}

	public T_exp buildIR() {
		return classBody.buildIR(class_id,extend_class);
	}
	
}



