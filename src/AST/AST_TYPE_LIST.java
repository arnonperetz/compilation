package AST;
import IRNodes.*;
import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;
//!!!!
public class AST_TYPE_LIST extends AST_TYPE{
	AST_TYPE t; AST_ID_LIST idl;
	public AST_TYPE_LIST(AST_TYPE t, AST_ID_LIST idl2) {
		this.t=t;
		this.idl=idl2;
	}
	public void addToClassEntries(String class_id) {
		for(String symbol : idl.list ){
			SemanticAnalayzer.addFieldToClassEntry(symbol,t.getType(),class_id);
		}
	}
	public boolean isValid(String class_id){
		for(String sym : idl.list ){
			SymbolHashEntry symbol = new SymbolHashEntry(sym,t.getType());
			SemanticAnalayzer.symbolTable.add(symbol);
		}
		return true;
	}

	public T_exp buildIR(){
		return null;
	}

}

