package AST;
import Main.SemanticAnalayzer;
import Main.HashMap;

public class AST_CLASS_DECL_EXTEND extends AST_CLASS_DECLARATION{

	public AST_CLASS_DECL_EXTEND(String class_id,String extend_class,AST_CLASS_BODY_LIST classBody){
		this.class_id=class_id;
		this.extend_class = extend_class;
		this.classBody = classBody;
	}
	
	public boolean isValid(String class_id){
		if(!super.isValid(class_id))
			throw new RuntimeException();
		HashMap map = SemanticAnalayzer.classEntries;
		if(map.get(this.extend_class)==null)
			throw new RuntimeException();
		return true;
	}
}
