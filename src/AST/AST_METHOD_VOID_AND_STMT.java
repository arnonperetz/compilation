package AST;

import java.util.ArrayList;

import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_VOID_AND_STMT extends AST_METHOD {
	String name; AST_STMT_LIST l;
	public AST_METHOD_VOID_AND_STMT(String name, AST_STMT_LIST l) {
		this.name=name;
		this.l=l;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, "void", new ArrayList<String>(),class_id);
	}
	/*open scope
	 * checks statement list 
	 * close scope 
	 * return */
	public boolean isValid(String class_id){
		SemanticAnalayzer.symbolTable.startScope();
		boolean rv=l.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	/*no type . return "void" */
	public String getType(){
		return "void";
	}
}
