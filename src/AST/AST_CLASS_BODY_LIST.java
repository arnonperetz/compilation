package AST;

import java.util.ArrayList;
import java.util.List;

import IRNodes.T_exp;

public class AST_CLASS_BODY_LIST extends AST_Node{
List<AST_CLASS_BODY> classBodyList;
	
	public AST_CLASS_BODY_LIST(){
		classBodyList = new ArrayList<AST_CLASS_BODY>();
	}
	public AST_CLASS_BODY_LIST(AST_CLASS_BODY cb) {
		classBodyList = new ArrayList<AST_CLASS_BODY>();
		Add(cb);
	}
	public void Add(AST_CLASS_BODY cb) {
		this.classBodyList.add(cb);
	}
	public void addClassBodyToHashEntry(String class_id) {
		for(AST_CLASS_BODY cb : classBodyList){
			cb.addToClassEntries(class_id);
		}
	}
	public boolean isValid(String class_id){
		for (AST_CLASS_BODY b : this.classBodyList){
			if(!b.isValid(class_id))
				throw new RuntimeException();
		}
		return true;
	}
	public T_exp buildIR(String class_id, String extend_class) {
		return null;
		// TODO Auto-generated method stub
		
	}
}
