package AST;

public class AST_CLASS_BODY extends AST_Node{
	AST_METHOD m=null;
	AST_FIELD f=null;
	public void addToClassEntries(String class_id) {
			if(m!=null)
				m.addToClassEntries(class_id);
			if(f!=null)
				f.addToClassEntries(class_id);
	}
	public boolean isValid(String class_id){
		if(m!=null&&!m.isValid(class_id))
			throw new RuntimeException();
		if(f!=null&&!f.isValid(class_id))
			throw new RuntimeException();
		return true;
	}
	
}
