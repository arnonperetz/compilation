package AST;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

public class AST_ARG_LIST extends AST_Node{
	List<AST_ARG> list;
	public AST_ARG_LIST(AST_ARG a2) {
		list = new ArrayList<AST_ARG>();
		Add(a2);
	}
	public void Add(AST_ARG a2) {
		list.add(a2);
	}
	public List<String> getFormals() {
		// TODO Auto-generated method stub
		List<String> rv = new ArrayList<String>();
		for(AST_ARG arg : list)
			rv.add(arg.t.getType());
		return rv;
	}
	public boolean isValid(String class_id){
		boolean rv = true;
		for(AST_ARG arg : list ){
			rv = rv && arg.isValid(class_id);
		}
		if(rv) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return null;
	}
}
