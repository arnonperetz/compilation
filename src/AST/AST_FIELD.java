package AST;

import Main.SemanticAnalayzer;

//!!!!
public class AST_FIELD {
	AST_TYPE_LIST tl;
	public AST_FIELD(AST_TYPE_LIST tl) {
		this.tl=tl;
	}
	public void addToClassEntries(String class_id) {
		tl.addToClassEntries(class_id);
	}
	public boolean isValid(String class_id){
		if(tl.isValid(class_id)) return true;
		throw new RuntimeException();
	}
	
}
