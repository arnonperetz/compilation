package AST;

import java.util.ArrayList;
import java.util.Map;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

//!!!!
public class AST_METHOD_TYPE_FORMALS_AND_STMT extends AST_METHOD {
	AST_TYPE t; String name;
	AST_FORMALS f; AST_STMT_LIST l;
	public AST_METHOD_TYPE_FORMALS_AND_STMT(AST_TYPE t, String name,
			AST_FORMALS f, AST_STMT_LIST l) {
		this.t=t;
		this.f=f;
		this.l=l;
		this.name=name;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, t.getType(), f.getFormals(),class_id);
	}
	/*open scope 
	 * check formals . if not valid return false
	 * checks statement list  with return type 
	 * close scope 
	 * return*/
	public boolean isValid(String class_id){
		
		SemanticAnalayzer.symbolTable.startScope();
		if(this.f.getArgList()!=null)
		for (Map.Entry<String, String> entry : this.f.getArgList().entrySet()){
			SymbolHashEntry symbol = new SymbolHashEntry(entry.getKey(),entry.getValue());
			SemanticAnalayzer.symbolTable.add(symbol);
		}
		if(f!=null && !f.isValid(class_id)) {
			SemanticAnalayzer.symbolTable.endScope();
			throw new RuntimeException();		}
		boolean rv = l.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	/*return type*/
	public String getType(){
		return t.getType();
	}
}
