package AST;

public class AST_STMT_WHILE extends AST_STMT
{
	public AST_EXP cond;
	public AST_STMT_LIST body;
	AST_STMT body2;
	/*******************/
	/*  CONSTRUCTOR(S) */
	/*******************/
	public AST_STMT_WHILE(AST_EXP cond,AST_STMT_LIST body)
	{
		this.cond = cond;
		this.body = body;
	}

	public AST_STMT_WHILE(AST_EXP cond2, AST_STMT body2) {
		// TODO Auto-generated constructor stub
		this.cond=cond2;
		this.body2=body2;
	}
	public boolean isValid(String class_id){
		if(!cond.isValid(class_id))
			throw new RuntimeException();
		if(body2!=null){
			if(!body2.isValid(class_id))
				throw new RuntimeException();
		}
		if(body!=null){
			if(body.isValid(class_id)) return true;
			throw new RuntimeException();
		}
		if(this.cond.getType().equals("INT")) return true;
		throw new RuntimeException();
	}
}