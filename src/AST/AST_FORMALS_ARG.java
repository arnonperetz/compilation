package AST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

//!!!!
public class AST_FORMALS_ARG extends AST_FORMALS{
	AST_TYPE t; String name;
	public AST_FORMALS_ARG(AST_TYPE t, String name) {
		this.t=t;
		this.name=name;
	}
	public List<String> getFormals(){
		List<String> rv = new ArrayList<String>();
		rv.add(t.getType());
		return rv;
	}
	public Map<String,String> getArgList(){
		Map<String,String> rv = new HashMap<String,String>();
		rv.put(this.name, this.t.getType());
		return rv;
	}
	public boolean isValid(String class_id){
		SymbolHashEntry symbol = new SymbolHashEntry(name,t.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
		return true;
	}

}
