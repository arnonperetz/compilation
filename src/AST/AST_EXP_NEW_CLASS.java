package AST;

import Main.SymbolHashEntry;

//!!!!
public class AST_EXP_NEW_CLASS extends AST_EXP {
	String class_id;
	public AST_EXP_NEW_CLASS(String class_id) {
		this.class_id=class_id;
	}
	public boolean isValid(String class_id_){
		if(Main.SemanticAnalayzer.classEntries.get(this.class_id)!=null) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return this.class_id;
	}
}
