package AST;
//!!!!
public class AST_STMT_CALL extends AST_STMT {
	AST_CALL c;
	public AST_STMT_CALL(AST_CALL c) {
		this.c=c;
	}
	public boolean isValid(String class_id) {
		if(c.isValid(class_id)) return true;
		throw new RuntimeException();
	}
}
