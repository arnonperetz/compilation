package AST;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

public class AST_ARG extends AST_Node{

	AST_TYPE t; String name;
	public AST_ARG(AST_TYPE t, String name) {
		this.t=t;
		this.name=name;
	}
	
	public String getType(){
		return t.getType();
	}
	public boolean isValid(String class_id){
		SymbolHashEntry symbol = new SymbolHashEntry(name,t.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
		return true;
	}
}
