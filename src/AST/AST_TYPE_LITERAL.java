package AST;
//!!!!

public class AST_TYPE_LITERAL extends AST_TYPE{
	int i; String id;
	public AST_TYPE_LITERAL(int i) {
		// TODO Auto-generated constructor stub
		this.i=i;
	}

	public AST_TYPE_LITERAL(int i, String id) {
		// TODO Auto-generated constructor stub
		this.i=i;
		this.id=id;
	}
	public String getType(){
		switch(i){
		case 0:
			return "INT";
		case 1:
			return "STRING";
		case 2:
			return id;
		case 3:
			return "VOID";
		}
		return null;
	}
	public boolean isValid(){
		return true;
	}
}
