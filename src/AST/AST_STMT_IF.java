package AST;

public class AST_STMT_IF extends AST_STMT
{
	public AST_EXP cond;
	public AST_STMT_LIST body=null;
	AST_STMT body2=null;
	/*******************/
	/*  CONSTRUCTOR(S) */
	/*******************/
	public AST_STMT_IF(AST_EXP cond,AST_STMT_LIST body)
	{
		this.cond = cond;
		this.body = body;
	}

	public AST_STMT_IF(AST_EXP cond2, AST_STMT body2) {
		this.cond=cond2;
		this.body2=body2;
	}
	public boolean isValid(String class_id){
		if(!cond.isValid(class_id))
			throw new RuntimeException();
		if(body2!=null){
			if(body2.isValid(class_id)) return true;
			throw new RuntimeException();
		}
		if(body!=null){
			if(body.isValid(class_id)) return true;
			throw new RuntimeException();
		}
		if(this.cond.getType().equals("IF")) return true;
		throw new RuntimeException();
		

	}
}