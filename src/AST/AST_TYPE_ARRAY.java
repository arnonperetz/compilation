package AST;
//!!!!
public class AST_TYPE_ARRAY extends AST_TYPE {
	AST_TYPE t;
	public AST_TYPE_ARRAY(AST_TYPE t) {
		// TODO Auto-generated constructor stub
		this.t=t;
	}
	public String getType(){
		String rv  ="[]";
		AST_TYPE curr = this.t;
		while(curr instanceof AST_TYPE_ARRAY){
			rv +="[]";
			curr = ((AST_TYPE_ARRAY)curr).t;
		}
		return curr.getType()+rv;
	}
	public boolean isValid(){
		return true;
	}
}
