package AST;

import java.util.ArrayList;
import java.util.List;

//!!!!
public class AST_CLASS_DECL_LIST extends AST_Node{

	List<AST_CLASS_DECLARATION> list;
	public AST_CLASS_DECL_LIST(AST_CLASS_DECLARATION cd) {
		list = new ArrayList<AST_CLASS_DECLARATION>();
		Add(cd);
	}

	public void Add(AST_CLASS_DECLARATION cd) {
		list.add(cd);
	}
	public boolean isValid(String class_id){
		for(AST_CLASS_DECLARATION cl : list)
			if(!cl.isValid(class_id))
				throw new RuntimeException();
		return true;
	}
}
