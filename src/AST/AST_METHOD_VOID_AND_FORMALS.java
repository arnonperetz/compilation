package AST;

import java.util.ArrayList;

import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_VOID_AND_FORMALS extends AST_METHOD {
	String name; AST_FORMALS f;
	public AST_METHOD_VOID_AND_FORMALS(String name, AST_FORMALS f) {
		this.name=name;
		this.f=f;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, "void", f.getFormals(),class_id);
	}
	/*opens scope
	 * checks formals 
	 * ends scope 
	 * return if formals are ok
	 * */
	public boolean isValid(String class_id){
		SemanticAnalayzer.symbolTable.startScope();
		boolean rv = f.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	/*returns "void" because there is no type*/
	public String getType(){
		return "void";
	}
}
