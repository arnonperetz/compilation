package AST;
//!!!!
public class AST_STMT_LOCATION_ASSIGN extends AST_STMT {
	AST_LOCATION loc; AST_EXP e;
	public AST_STMT_LOCATION_ASSIGN(AST_LOCATION loc, AST_EXP e) {
		// TODO Auto-generated constructor stub
		this.loc=loc;
		this.e=e;
	}
	public boolean isValid(String class_id){
		if(!e.isValid(class_id))
			throw new RuntimeException();
		if(!loc.isValid(class_id))
			throw new RuntimeException();
		if(loc.getType().equals(e.getType())) return true;
		throw new RuntimeException();
	}

}
