package AST;
//!!!!
public class AST_EXP_CALL extends AST_EXP {
	AST_CALL c;
	
	public AST_EXP_CALL(AST_CALL c) {
		this.c=c;
	}
	
	public boolean isValid(String class_id) {
		if(c.isValid(class_id)) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return c.getType();
	}
}

