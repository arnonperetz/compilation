package AST;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

//!!!!
public class AST_STMT_DECL_AND_ASSIGN extends AST_STMT {
	AST_TYPE t; String id; AST_EXP e;
	public AST_STMT_DECL_AND_ASSIGN(AST_TYPE t, String id, AST_EXP e) {
		this.t=t;
		this.id=id;
		this.e=e;
	}
	public boolean isValid(String class_id){
		SymbolHashEntry symbol = new SymbolHashEntry(this.id,e.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
		if(!e.isValid(class_id))
			throw new RuntimeException();
		if(SemanticAnalayzer.compareTypesForClassInstitaion(t.getType(),e.getType())) return true;
		throw new RuntimeException();
	}

}
