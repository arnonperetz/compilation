package AST;
//!!!!
public class AST_EXP_LOCATION extends AST_EXP {
	AST_LOCATION loc;
	
	public AST_EXP_LOCATION(AST_LOCATION loc) {
		this.loc=loc;
	}

	public boolean isValid(String class_id){
		if(loc.isValid(class_id)) return true;
		throw new RuntimeException();
	}
	public String getType(){
		return loc.getType();
	}
}