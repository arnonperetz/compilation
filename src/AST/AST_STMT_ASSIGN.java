package AST;

public class AST_STMT_ASSIGN extends AST_STMT
{
	public AST_EXP exp;
	public AST_VAR var;

	/*******************/
	/*  CONSTRUCTOR(S) */
	/*******************/
	public AST_STMT_ASSIGN(AST_VAR var,AST_EXP exp)
	{
		this.var = var;
		this.exp = exp;
	}
	public boolean isValid(String class_id) {
		if(!exp.isValid(class_id))
			throw new RuntimeException();
		if(var.getType().equals(exp.getType())) return true;
		throw new RuntimeException();
	}
}