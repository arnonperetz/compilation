package AST;

import java.util.ArrayList;
import java.util.List;

//!!!!

public class AST_EXP_LIST extends AST_Node{

	public AST_EXP head;
	public AST_EXP_LIST tail;
	List<AST_EXP> list;
	
	/*
	public AST_EXP_LIST(AST_EXP head,AST_EXP_LIST tail)
	{
		this.head = head;
		this.tail = tail;
	}
	*/
	
	public AST_EXP_LIST(AST_EXP s) 
	{
		list = new ArrayList<AST_EXP>();
		add(s);
	}
	
	public void add(AST_EXP s) 
	{
		list.add(s);
	}
	
	public boolean isValid(String class_id)
	{
		boolean rv =true;
		for(AST_EXP e : this.list){
			rv = rv && e.isValid(class_id);
		}
		if(rv) return true;
		throw new RuntimeException();
		
		/*
		boolean rv = head.isValid();
		AST_EXP_LIST curr= tail;
		while(curr!=null){
			rv = rv & curr.head.isValid();
			curr=curr.tail;
		}
		return rv;
		*/
	}
	public List<String> getArgsType(){
		List<String> rv = new ArrayList<String>();
		for(int i=this.list.size()-1;i>=0;i--)
			rv.add(this.list.get(i).getType());
		return rv;
	}

}



