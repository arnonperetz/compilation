package AST;

import Main.SemanticAnalayzer;

//!!!!
public class AST_METHOD_VOID_FORMALS_AND_STMT extends AST_METHOD {
	String name; AST_FORMALS f;
	AST_STMT_LIST l;
	public AST_METHOD_VOID_FORMALS_AND_STMT(String name, AST_FORMALS f,
			AST_STMT_LIST l) {
		this.name=name;
		this.l=l;
		this.f=f;
	}
	public void addToClassEntries(String class_id) {
		this.class_id = class_id;
		SemanticAnalayzer.addMethodToClassEntry(this.name, "void", f.getFormals(),class_id);
	}
	/*open scope 
	 * check formals . if not valid return false
	 * checks statement list
	 * close scope 
	 * return*/
	public boolean isValid(String class_id){
		
		SemanticAnalayzer.symbolTable.startScope();
		if(!f.isValid(class_id)) {
			SemanticAnalayzer.symbolTable.endScope();
			throw new RuntimeException();
		}
		boolean rv = l.isValid(class_id);
		SemanticAnalayzer.symbolTable.endScope();
		if(rv) return true;
		throw new RuntimeException();
	}
	/*return "void"*/
	public String getType(){
		return "void";
	}
}
