package AST;

import java.util.ArrayList;
import java.util.List;

//!!!!
public class AST_ID_LIST extends AST_Node {

	List<String> list;
	public AST_ID_LIST(String id) {
		list= new ArrayList<String>();
		Add(id);
	}
	public AST_ID_LIST() {
		list= new ArrayList<String>();
	}
	public void Add(String id) {
		this.list.add(id);
		
	}
	/*always true*/
	public boolean isValid(){
		return true;
	}
	/*not needed realy */
	public String getType(){
		return null;
	}


}
