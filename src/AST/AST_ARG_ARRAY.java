package AST;

import Main.SemanticAnalayzer;
import Main.SymbolHashEntry;

public class AST_ARG_ARRAY extends AST_ARG {

	public AST_ARG_ARRAY(AST_TYPE t, String name) {
		super(t, name);
		// TODO Auto-generated constructor stub
	}
	public boolean isValid(){
		SymbolHashEntry symbol = new SymbolHashEntry(name,t.getType());
		SemanticAnalayzer.symbolTable.add(symbol);
		return true;
	}
	public String getType(){
		return t.getType();
	}
}
