package IRNodes;

public class T_Function {
	T_exp prologue;
	T_exp body;
	T_exp epilogue;
	T_Label name;
	public T_Function(T_exp prologue, T_exp body, T_exp epilogue, T_Label name){
		this.prologue=prologue;
		this.body=body;
		this.epilogue=epilogue;
		this.name=name;
	}
}
