package IRNodes;

public class T_CJump {
	String op;
	T_exp left;
	T_exp right;
	T_Label ifTrue;
	T_Label ifFalse;
	public T_CJump(String op,T_exp left,T_exp right,T_Label ifTrue,T_Label ifFalse){
		op = op;
		left = left;
		right = right;
		ifTrue = ifTrue;
		ifFalse = ifFalse;
	}
}
